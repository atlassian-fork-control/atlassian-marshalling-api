package com.atlassian.marshalling.api;

import com.atlassian.annotations.PublicApi;

import javax.annotation.concurrent.Immutable;

import static java.util.Objects.requireNonNull;

/**
 * Convenience class for holding a reciprocal {@link Marshaller} and {@link Unmarshaller} instances.
 * By convention, the output produced by the {@link Marshaller} (retrieved via {@link #getMarshaller()}) should be able
 * to be processed by the {@link Unmarshaller} (retrieved via {@link #getUnmarshaller()}).
 *
 * @param <T> the type that can be converted
 * @since 1.0.0
 */
@Immutable
@PublicApi
public class MarshallingPair<T> {
    private final Marshaller<T> marshaller;
    private final Unmarshaller<T> unmarshaller;

    /**
     * Creates an instance.
     * @param marshaller the marshaller instance.
     * @param unmarshaller the unmarshaller instance.
     */
    public MarshallingPair(Marshaller<T> marshaller, Unmarshaller<T> unmarshaller) {
        this.marshaller = requireNonNull(marshaller);
        this.unmarshaller = requireNonNull(unmarshaller);
    }

    /**
     * @return the marshaller
     */
    public Marshaller<T> getMarshaller() {
        return marshaller;
    }

    /**
     * @return the unmarshaller
     */
    public Unmarshaller<T> getUnmarshaller() {
        return unmarshaller;
    }
}
