package com.atlassian.marshalling.api;

import com.atlassian.annotations.PublicApi;

/**
 * Represents a failure occurred when marshalling data.
 *
 * @since 1.0
 */
@PublicApi
public class MarshallingException extends RuntimeException {
    /**
     * Creates an instance with the supplied message.
     *
     * @param message message for the reason
     */
    public MarshallingException(String message) {
        super(message);
    }

    /**
     * Creates an instance with the supplied message and causing {@link Throwable}.
     *
     * @param message message for the reason
     * @param cause   the causing {@link Throwable}
     */
    public MarshallingException(String message, Throwable cause) {
        super(message, cause);
    }
}
